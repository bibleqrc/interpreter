#ifndef ENV_HPP
#define ENV_HPP

#include <map>
#include <string>

class Object;
class DictObject;

class StackFrame {
private:
    std::map<std::string, Object*>* _var_table;

public:
    StackFrame() {
        _var_table = new std::map<std::string, Object*>();
    }

    // Create a stack frame from an envrionment of LambdaObject.
    StackFrame(DictObject* env);

    ~StackFrame();

    std::map<std::string, Object*>* get_map() {
        return _var_table;
    }

    void save_var(const char* name, Object* value);

    Object* lookup(const char* name);
};

#endif
