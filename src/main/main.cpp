#include "parser/parser.hpp"
#include "runtime/universe.hpp"
#include "util/bufferedInputStream.hpp"

int main(int argc, char** argv) {
    if (argc <= 1) {
        printf("interpreter need a parameter : filename\n");
        return 0;
    }

    Universe::genesis();

    BufferedInputStream stream(argv[1]);
    eval(&stream);

    Universe::destroy();
    return 0;
}

