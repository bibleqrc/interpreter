#ifndef DICT_OBJECT_HPP
#define DICT_OBJECT_HPP

#include "object.hpp"

#include <map>

class StackFrame;

class PairObject : public Object {
private:
    Object* _key;
    Object* _value;

    PairObject* _left;
    PairObject* _right;
    PairObject* _parent;

public:
    PairObject(Object* key, Object* value, PairObject* p):
        _key(key), _value(value), _left(NULL), _right(NULL), _parent(p) {}
        
    int compare(Object* key);
    
    Object* key() { return _key; }
    Object* value() { return _value; }
    
    void set_key(Object* key) { _key = key; }
    void set_value(Object* v) { _value = v; }
    
    PairObject* left() { return _left; }
    PairObject* right() { return _right; }
    PairObject* parent() { return _parent; }
    
    void set_left(PairObject* p) { _left = p; }
    void set_right(PairObject* p) { _right = p; }
    void set_parent(PairObject* p) { _parent = p; }
    
    void copy_into_map(std::map<std::string, Object*>* map);

    void print();
};

class DictObject : public Object {
private:
    PairObject* _root;
    
public:
    DictObject() : _root(NULL) {}

    static DictObject* from_stack_frame(StackFrame*);
    
    void copy_into_map(std::map<std::string, Object*>* map);
    
    void put(Object* key, Object* value);
    Object* find(Object* key);
    
    void save_var(const char* name, Object* value);
    Object* lookup(const char* name);
    
    void print();
};

#endif
