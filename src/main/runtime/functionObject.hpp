#ifndef FUNCTION_OBJECT_HPP
#define FUNCTION_OBJECT_HPP

#include "runtime/object.hpp"
#include <vector>

typedef Object* (*NativeFunc)(std::vector<Object*>& args);

class NativeFunctionObject : public Object {
private:
    NativeFunc _func;

public:
    NativeFunctionObject(NativeFunc f);

    NativeFunc get_func() { return _func; }

    void print();
};

Object* sys_print(std::vector<Object*>& args);
Object* sys_println(std::vector<Object*>& args);

#endif
