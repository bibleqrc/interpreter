#ifndef KLASS_HPP
#define KLASS_HPP

#include <cstdlib>

class Klass {
private:
    const char* _name;

public:
    Klass(const char* n): _name(n) {}
};

class IntKlass : public Klass {
private:
    IntKlass(): Klass("Int") {}

public:
    static IntKlass* get_instance() {
        static IntKlass inst;
        return &inst;
    }
};

class CharKlass : public Klass {
private:
    CharKlass(): Klass("Char") {}

public:
    static CharKlass* get_instance() {
        static CharKlass inst;
        return &inst;
    }
};

class StringKlass : public Klass {
private:
    StringKlass(): Klass("String") {}

public:
    static StringKlass* get_instance() {
        static StringKlass inst;
        return &inst;
    }
};

class ClosureKlass : public Klass {
private:
    ClosureKlass(): Klass("Closure") {}

public:
    static ClosureKlass* get_instance() {
        static ClosureKlass inst;
        return &inst;
    }
};

class NativeFunctionKlass : public Klass {
private:
    NativeFunctionKlass(): Klass("NativeFunction") {}

public:
    static NativeFunctionKlass* get_instance() {
        static NativeFunctionKlass inst;
        return &inst;
    }
};

#endif

