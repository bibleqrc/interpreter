var n = 2;
println(1 if n == 1 else 2 if n == 2 else 3);

var f:Int->Int = ($x:Int => x + 1) if n == 1 else ($x:Int => x * 2);
println(f(4));
n = 1;
f = ($x:Int => x + 1) if n == 1 else ($x:Int => x * 2);
println(f(4));

println("False" if 2 == 1 && n == 1 else "True");

println(2 == 1);
println(!2 == 1);
println(!(2 == 1));
println(1 == 1 && 2 == 2);
println(1 == 1 && 2 == 3);
println(1 == 1 || 2 == 3);

