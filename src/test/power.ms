var square = $x:Int => x * x;
var pow = $fp:Int->Int->Int => $x:Int => $n:Int => 1 if n == 0 else (fp(x)(n-1) * x if n & 1 == 1 else square(fp(x)(n>>1)));
var U : Any->Any;
U = $u:Any =>u(u);
var Y = $f:Any => U($x:Any => f($v:Any => x(x)(v)));

println(Y(pow)(2)(10));
